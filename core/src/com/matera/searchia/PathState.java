package com.matera.searchia;

import com.badlogic.gdx.math.Vector2;



/* Stato del problema contenuto in un nodo*/
public class PathState implements State {
    private final World world;
    private final Vector2 robotPosition;
    private final Vector2 goalPosition;

    public PathState(World world, Vector2 robotPosition, Vector2 goalPosition) {
        this.world = world;
        this.robotPosition = robotPosition;
        this.goalPosition = goalPosition;
    }

    /* Act simula un'azione e ritorna lo stato futuro se si eseguisse l'azione stessa
    *  (creazione dei nodi figli a partire da uno stato) */
    @Override
    public State act(Action prevAction, Action action) {
        Robot.RobotAction prevRobotAction = (Robot.RobotAction) prevAction;
        Robot.RobotAction robotAction = (Robot.RobotAction) action;
        World newWorld = new World(world);
        Vector2 newPosition;

        switch (robotAction){
            case RIGHT:
                if (prevRobotAction != Robot.RobotAction.LEFT){
                    newPosition = robotPosition.cpy();
                    newPosition.add(1, 0);
                    if (world.isAValidPosition(newPosition))
                        return new PathState(newWorld, newPosition, goalPosition);
                }
                return null;

            case LEFT:
                if (prevRobotAction != Robot.RobotAction.RIGHT){
                    newPosition = robotPosition.cpy();
                    newPosition.add(-1, 0);
                    if (world.isAValidPosition(newPosition))
                        return new PathState(newWorld, newPosition, goalPosition);
                }
                return null;

            case UP:
                if (prevRobotAction != Robot.RobotAction.DOWN){
                    newPosition = robotPosition.cpy();
                    newPosition.add(0, 1);
                    if (world.isAValidPosition(newPosition))
                        return new PathState(newWorld, newPosition, goalPosition);
                }
                return null;

            case DOWN:
                if (prevRobotAction != Robot.RobotAction.UP){
                    newPosition = robotPosition.cpy();
                    newPosition.add(0, -1);
                    if (world.isAValidPosition(newPosition))
                        return new PathState(newWorld, newPosition, goalPosition);
                }
                return null;

            default:
                return null;
        }
    }

    /* Controllo del raggiungimento del goal */
    @Override
    public boolean isGoal() {
        return getProblemDimension() == 0;
    }

    /* Ritorna un calcolo sulla dimensione del problema nello stato attuale */
    @Override
    public int getProblemDimension() {
        // manhattan distance
        return Math.abs((int)(robotPosition.x - goalPosition.x)) + Math.abs((int)(robotPosition.y - goalPosition.y));
    }
}