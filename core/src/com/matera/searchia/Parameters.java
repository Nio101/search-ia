package com.matera.searchia;

import com.matera.searchia.Tree.SearchAlg;
import com.badlogic.gdx.math.Vector2;

/* Parametri di esecuzione (ricompilare ad ogni cambiamento) */

public class Parameters {

    public static final SearchAlg algorithm = SearchAlg.ASTAR;

    public static final int worldRows = 7;
    public static final int worldCols = 7;

    public static final int robotX = 0;
    public static final int robotY = 0;

    public static final int goalX = 4;
    public static final int goalY = 6;

    // Probabilità che una cella non sia un muro
    public static final float groundProbability = 0.8f;
}
