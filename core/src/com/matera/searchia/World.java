package com.matera.searchia;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;


/* Il mondo di gioco */
public class World {

    public static final int STATUS_NORMAL = 0;
    public static final int STATUS_WALL = 1;

    public static float originX;
    public static float originY;
    public static int cellSize;
    private final int rows;
    private final int cols;
    private int grid[][];


    /* Clone a world */
    public World(World toClone){
        this.rows = toClone.getHeight();
        this.cols = toClone.getWidth();
        grid = new int[rows][cols];
        for(int i=0; i<rows; i++)
            grid[i] = toClone.grid[i].clone();
    }

    public World(int rows, int cols){
        this.rows = rows;
        this.cols = cols;
        cellSize = Math.min(50/rows, 50/cols);
        originX = -((float) cols)/2;
        originY = -((float) rows)/2;
        grid = new int[rows][cols];

        for (int i=0; i<rows; i++)
            for (int j=0; j<cols; j++)
                grid[i][j] = Math.random() > Parameters.groundProbability ? STATUS_WALL : STATUS_NORMAL;
    }


    public int getWidth(){
        return cols;
    }

    public int getHeight(){
        return rows;
    }

    public int getStatus(int x, int y){
        return grid[y][x];
    }

    public boolean isAValidPosition(Vector2 position){
        int x = (int) position.x;
        int y = (int) position.y;

        if (!(x >= 0 && x < cols))
            return false;
        else if (!(y >= 0 && y < rows))
            return false;
        else if (grid[y][x] != STATUS_NORMAL)
            return false;

        return true;
    }

    public void init(Vector2 robot, Vector2 goal){
        int x = (int) robot.x;
        int y = (int) robot.y;
        if (x >= 0 && x < cols && y >= 0 && y < rows)
            grid[y][x] = STATUS_NORMAL;

        x = (int) goal.x;
        y = (int) goal.y;
        if (x >= 0 && x < cols && y >= 0 && y < rows)
            grid[y][x] = STATUS_NORMAL;
    }
}
