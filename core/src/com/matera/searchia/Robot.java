package com.matera.searchia;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

/* Implementazione di un agente.
 * È rappresentato da una posizione e si limita ad eseguire
 * le azioni calcolate dall'algoritmo in precedenza. */
public class Robot implements Agent {
    private static final String TAG = "ROBOT";
    private final Vector2 position;
    private final Vector2 worldDimension;

    public enum RobotAction implements Action{
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    public Robot(World world, int r, int c){
        this.position = new Vector2(c, r);
        this.worldDimension = new Vector2(world.getWidth(), world.getHeight());
    }


    @Override
    public void update(Action action) {
        RobotAction robotAction= (RobotAction) action;

        switch (robotAction){
            case RIGHT:
                Gdx.app.log(TAG, "Right");
                if (position.x < worldDimension.x-1)
                    position.add(1, 0);
                break;

            case LEFT:
                Gdx.app.log(TAG, "Left");
                if (position.x > 0)
                    position.add(-1, 0);
                break;

            case UP:
                Gdx.app.log(TAG, "Up");
                if (position.y < worldDimension.y-1)
                    position.add(0, 1);
                break;

            case DOWN:
                Gdx.app.log(TAG, "Down");
                if (position.y > 0)
                    position.add(0, -1);
                break;
        }
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }
}
