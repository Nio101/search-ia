package com.matera.searchia;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;


/* Classe che rappresenta un nodo dell'albero.
*  Oltre ai vari puntatori contiene lo stato del problema rappresentato dal nodo stesso.*/
public class PathNode implements Node {
    private final State state;
    private final Node parent;
    private final Action prevAction;
    private final int depth;
    private final float pathCost;

    public PathNode(State state, Node parent, Action prevAction, int depth, float pathCost) {
        this.state = state;
        this.parent = parent;
        this.prevAction = prevAction;
        this.depth = depth;
        this.pathCost = pathCost;
    }


    /* Espansione del nodo: per ogni azione possibile viene creato un nodo figlio. */
    @Override
    public List<Node> expand() {
        List<Node> childrens = new ArrayList<Node>();

        for (Action action: Robot.RobotAction.values()){
            State next = state.act(prevAction, action);
            if (next != null) {
                Node s = new PathNode(next, this, action, depth+1, pathCost+1);
                childrens.add(s);
            }
        }
        return childrens;
    }

    @Override
    public boolean isGoal() {
        return state.isGoal();
    }

    @Override
    public Node getParent() {
        return parent;
    }

    @Override
    public Action getAction() {
        return prevAction;
    }

    @Override
    public int getDepth() {
        return depth;
    }

    @Override
    public float getPathCost() {
        return pathCost;
    }

    /* Euristica di A* */
    @Override
    public float f() {
        return g() + h();
    }

    private float g() {
        return pathCost;
    }

    private float h() {
        return state.getProblemDimension();
    }
}
