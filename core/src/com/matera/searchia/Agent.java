package com.matera.searchia;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;


public interface Agent {

    void update(Action action);

    Vector2 getPosition();

}


