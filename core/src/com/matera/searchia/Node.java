package com.matera.searchia;

import java.util.List;



public interface Node {
    
    List<Node> expand();

    boolean isGoal();

    Node getParent();

    Action getAction();

    int getDepth();

    float getPathCost();

    float f();

}
