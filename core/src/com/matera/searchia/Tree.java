package com.matera.searchia;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;


/* Implementazione di un albero e degli algoritmi di ricerca */
public class Tree {
    public static enum SearchAlg {BFS, DFS, IDS, ASTAR}
    public static enum ResultState {OK, FAILURE, CUTOFF}
    private final LinkedList<Node> fringe;
    private final Node root;


    public Tree(Node root){
        fringe = new LinkedList<Node>();
        this.root = root;
    }


    public Result search(SearchAlg alg){
        switch (alg){
            case BFS: return bfs();
            case DFS: return dfs();
            case IDS: return ids();
            case ASTAR: return aStar();
            default: return ids();
        }
    }



    private Result bfs(){
        Node node;
        fringe.addFirst(root);

        while(true){
            if (fringe.isEmpty())
                return null;
            node = fringe.pollFirst();
            if (node.isGoal())
                return new Result(node);
            fringe.addAll(node.expand());
        }
    }


    private Result dfs(){
        Node node;
        fringe.addFirst(root);

        while(true){
            if (fringe.isEmpty())
                return null;
            node = fringe.pollFirst();
            if (node.isGoal())
                return new Result(node);

            fringe.addAll(0, node.expand());
        }
    }



    private Result ids(){
        int depth = 0;
        Result result;

        do{
            Gdx.app.log("TREE", "depth: " + depth);
            result = recursiveDls(root, depth);
            depth++;
        } while(result.state == ResultState.CUTOFF);

        return result;
    }


    private Result recursiveDls(Node node, int limit){
        Result result;
        boolean cutoffOccurred = false;

        if (node.isGoal())
            return new Result(node);
        else if (node.getDepth() == limit)
            return new Result(ResultState.CUTOFF);
        else
            for (Node children: node.expand()){
                result = recursiveDls(children, limit);
                if (result != null){
                    if (result.state == ResultState.CUTOFF)
                        cutoffOccurred = true;
                    else
                        return result;
                }
            }

        return cutoffOccurred ? new Result(ResultState.CUTOFF) : new Result(ResultState.FAILURE);
    }



    private Result aStar(){
        fringe.addFirst(root);
        Node node;

        while(true){
            if (fringe.isEmpty())
                return new Result(ResultState.FAILURE);
            node = Collections.min(fringe, fComparator);
            fringe.remove(node);
            if (node.isGoal())
                return new Result(node);
            fringe.addAll(node.expand());
        }
    }

    /* comparatore di nodi, serve per trovare il nodo della lista che ha euristica minore */
    Comparator<Node> fComparator = new Comparator<Node>() {
        @Override
        public int compare(Node n1, Node n2) {
            return (int) n1.f() - (int) n2.f();
        }
    };


    /* Result è una classe che ingloba un risultato ed un nodo.
    *  Il risultato può essere: ok, failure oppure cutoff */
    public class Result {
        public final ResultState state;
        public final Node node;

        public Result(ResultState state){
            this.state = state;
            this.node = null;
        }

        public Result(Node node){
            this.state = ResultState.OK;
            this.node = node;
        }
    }
}

