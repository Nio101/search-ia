package com.matera.searchia;

/* A state representation */
public interface State {

    State act(Action prevAction, Action action);

    boolean isGoal();

    int getProblemDimension();

}
