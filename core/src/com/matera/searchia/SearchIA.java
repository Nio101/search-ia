package com.matera.searchia;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.matera.searchia.Tree.Result;
import com.matera.searchia.Tree.ResultState;

import java.util.LinkedList;


public class SearchIA extends ApplicationAdapter {
    private static Texture robotTex;
    private static Texture cellTex;
    private static Texture wallTex;
    private static Texture goalTex;
    private static Texture noSolutionTex;

    private static final int UPDATE_PERIOD = 1;
    private OrthographicCamera cam;
    private SpriteBatch batch;
    private World world;
    private Agent robot;
    private Vector2 goal;
    private LinkedList<Action> toDo;
    private float timer;
    private boolean failure;

    /* Metodo invocato all'avvio.
     * Inizializza i dati e ricerca una soluzione al problema. */
    @Override
    public void create () {
        robotTex = new Texture("robot.png");
        cellTex = new Texture("ground.png");
        wallTex = new Texture("wall.png");
        goalTex = new Texture("goal.png");
        noSolutionTex = new Texture("no_solutions.png");

        cam = new OrthographicCamera(96, 60);
        batch = new SpriteBatch();
        toDo = new LinkedList<Action>();

        world = new World(Parameters.worldCols, Parameters.worldRows);
        robot = new Robot(world, Parameters.robotX, Parameters.robotY);
        goal = new Vector2(Parameters.goalX, Parameters.goalY);
        world.init(robot.getPosition(), goal);

        if (world.isAValidPosition(robot.getPosition()) && world.isAValidPosition(goal)) {

            /* Creazione di un nuovo albero */
            Node root = new PathNode(new PathState(world, robot.getPosition(), goal), null, null, 0, 0);
            Tree statesTree = new Tree(root);

            /* Ricerca di una soluzione nell'albero con l'algoritmo scelto */
            Result goal = statesTree.search(Parameters.algorithm);

            /* goal è l'ultimo stato che l'agente dovrebbe raggiungere,
             * quindi ora il programma ricostruisce a ritroso la lista delle azioni
             * finchè non trova la radice */
            if (goal.state != ResultState.FAILURE) {
                failure = false;
                Node node = goal.node;

                while (node.getParent() != null) {
                    toDo.push(node.getAction());
                    node = node.getParent();
                }
            } else
                failure = true;
        }
        else {
            Gdx.app.error("SEARCH", "Invalid parameteres");
            Gdx.app.exit();
        }
    }


    /* Metodo di rendering. Invocato dal programma ogni volta che è necessario aggiornare lo schermo.
    *  (molte volte al secondo) */
    @Override
    public void render () {
        update(Gdx.graphics.getDeltaTime());
        draw();
    }

    /* Esecuzione delle azioni da svolgere calcolate in precedenza */
    private void update(float dt){
        timer += dt;
        // Una volta al secondo
        if (timer > UPDATE_PERIOD && toDo.size() > 0) {
            robot.update(toDo.pop());
            timer = 0;
        }
    }

    /* Disegno delle grafiche tramite OpenGL ed utilità */
    private void draw() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(cam.combined);
        batch.begin();
        for (int i = 0; i < world.getWidth(); i++) {
            for (int j = 0; j < world.getHeight(); j++) {
                switch (world.getStatus(i, j)) {
                    case World.STATUS_NORMAL:
                        batch.draw(cellTex, (World.originX + i) * World.cellSize, (World.originY + j) * World.cellSize, World.cellSize, World.cellSize);
                        break;
                    case World.STATUS_WALL:
                        batch.draw(wallTex, (World.originX + i) * World.cellSize, (World.originY + j) * World.cellSize, World.cellSize, World.cellSize);
                        break;
                }
            }
        }
        batch.draw(
                goalTex,
                (World.originX + goal.x) * World.cellSize,
                (World.originY + goal.y) * World.cellSize,
                World.cellSize, World.cellSize
        );
        batch.draw(
                robotTex,
                (World.originX + robot.getPosition().x) * World.cellSize,
                (World.originY + robot.getPosition().y) * World.cellSize,
                World.cellSize, World.cellSize
        );

        if (failure)
            batch.draw(noSolutionTex, -36, -9, 72, 18);

        batch.end();
    }
}
