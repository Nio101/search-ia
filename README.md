# README #

IA search algorithms implementation in Java / LibGDX.

### Download executable ###
[SearchIA.jar](https://bitbucket.org/Nio101/search-ia/downloads/SearchIA.jar)

### Compile and run project from source ###

```
#!bash

./gradlew desktop:run
```

### Compile and create a Jar executable ###

```
#!bash
./gradlew desktop:dist

```
Jar will be created in ./desktop/build/libs/


### Compile and create an Apk for Android###

```
#!bash
./gradlew android:assemble

```
Apk will be created in ./android/build/apk/
